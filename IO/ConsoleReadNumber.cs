﻿using System;

namespace SOLID_GuessANumber.IO
{
    internal class ConsoleReadNumber : IWritableReadable<string, IReadable>
    {
        public T Read<T>()
        {
            while (true)
            {
                string input = "";

                try
                {
                    input = Console.ReadLine();

                    dynamic result = Convert.ChangeType(input, typeof(T));

                    return result;
                }
                catch (Exception ex)
                {
                    if (input == "exit")
                    {
                        throw new Exception("Terminated by user", ex);
                    }
                    else
                    {
                        Console.Write("Wrong value, try again: ");
                    }
                }
            }
        }

        public IReadable Write(string message)
        {
            Console.Write(message);

            return this;
        }
    }
}
