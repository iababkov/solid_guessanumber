﻿namespace SOLID_GuessANumber.IO
{
    internal interface IWritable<T1, T2>
    {
        T2 Write(T1 data);
    }
}
