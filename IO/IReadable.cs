﻿namespace SOLID_GuessANumber.IO
{
    internal interface IReadable
    {
        T Read<T>();
    }
}
