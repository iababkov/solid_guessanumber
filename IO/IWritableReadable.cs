﻿namespace SOLID_GuessANumber.IO
{
    internal interface IWritableReadable<T1, T2> : IWritable<T1, T2>, IReadable
    {
    }
}
