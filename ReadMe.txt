﻿https://gitlab.com/iababkov/solid_guessanumber
Реализовал принципы SOLID в настройках для игры (диапазон значений, загаданное число, количество попыток)

1. Single Responsibility Principle
Класс InitSettings отвечает только за инициализацию настроек:
https://gitlab.com/iababkov/solid_guessanumber/-/blob/master/Settings/InitSettings.cs

2. Open/Closed Principle
Класс SettingsContractRandom расширяет класс SettingsContract добавляя параметр RandomNumber:
https://gitlab.com/iababkov/solid_guessanumber/-/blob/master/Settings/SettingsContractRandom.cs

3. Liskov Substitution Principle
При инициализации класса InitSettings в него передаётся экземпляр класса ConsoleReadNumber,
но при желании туда можно передать его наследника,
например, который будет логировать каждое введённое значение (перекрыть метод Read):
https://gitlab.com/iababkov/solid_guessanumber/-/blob/master/Program.cs#L11
https://gitlab.com/iababkov/solid_guessanumber/-/blob/master/IO/ConsoleReadNumber.cs

4. Interface Segregation Principle
Интерфейс IWritableReadable наследует 2 других - IWritable и IReadable:
https://gitlab.com/iababkov/solid_guessanumber/-/blob/master/IO/IWritableReadable.cs

5. Dependency Inversion Principle
В качестве параметра конструктора класса InitSettings используется интерфейс IWritableReadable,
в текущей реализации класс ConsoleReadNumber выводит текстовое приглашение и ожидает ввода с консоли,
но можно, к примеру, создать иную реализацию, в которой метод Read считывает данные из файла.