﻿using System;
using SOLID_GuessANumber.Settings;

namespace SOLID_GuessANumber.Main
{
    class Game
    {
        private readonly SettingsContractRandom _settings;

        public Game(SettingsContractRandom settings)
        {
            _settings = settings;
        }

        public void Start()
        {
            byte attemptCounter = 1;
            bool keepPlaying = true;

            while (keepPlaying)
            {
                Console.Write($"Attempt {attemptCounter}: ");

                if (Byte.TryParse(Console.ReadLine(), out byte attemptNumber))
                {
                    if (attemptNumber >= _settings.Range.From && attemptNumber < _settings.Range.To)
                    {
                        if (attemptNumber == _settings.RandomNumber)
                        {
                            Console.WriteLine("This game is yours!");
                            keepPlaying = false;
                        }
                        else
                        {
                            if (attemptCounter < _settings.MaxAttempts)
                            {
                                Console.WriteLine(attemptNumber < _settings.RandomNumber ? "Try larger number." : "Try smaller number.");
                                attemptCounter++;
                            }
                            else
                            {
                                Console.WriteLine("You have lost!");
                                keepPlaying = false;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Number must be more or equal to {_settings.Range.From} and less than {_settings.Range.To}, reenter again.");
                    }
                }
                else
                {
                    Console.WriteLine("Not a number, reenter again.");
                }
            }
        }
    }
}
