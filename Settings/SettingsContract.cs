﻿using System;

namespace SOLID_GuessANumber.Settings
{
    class SettingsContract
    {
        private readonly ByteRange _range;
        private readonly byte _maxAttempts;

        public ByteRange Range { get { return _range; } }
        public byte MaxAttempts { get { return _maxAttempts; } }

        public SettingsContract(ByteRange range, byte maxAttempts)
        {
            if (range.Validate())
            {
                _range = range;
            }
            else
            {
                throw new Exception($"The range ({range.From}...{range.To}) is incorrect");
            }

            _maxAttempts = maxAttempts;
        }
    }
}
