﻿using System;

namespace SOLID_GuessANumber.Settings
{
    class ByteRange : IRange<Byte>
    {
        public Byte From { get; set; }

        public Byte To { get; set; }

        public bool Validate()
        {
            if (From >= To)
            {
                Console.WriteLine($"From range value {From} must be less than To range value {To}");
                return false;
            }

            return true;
        }
    }
}
