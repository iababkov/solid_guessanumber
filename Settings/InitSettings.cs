﻿using SOLID_GuessANumber.IO;

namespace SOLID_GuessANumber.Settings
{
    class InitSettings
    {
        private readonly SettingsContractRandom _settings;

        public SettingsContractRandom Settings { get { return _settings; } }

        public InitSettings(IWritableReadable<string, IReadable> inputSource)
        {
            ByteRange range = new();
            range.To = inputSource.Write("Enter integer limit to generate a random number: ").Read<byte>();

            _settings = new SettingsContractRandom(range, inputSource.Write("Enter maximum number of attempts: ").Read<byte>());
        }
    }
}
