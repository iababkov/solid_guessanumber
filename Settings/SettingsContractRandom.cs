﻿using System;

namespace SOLID_GuessANumber.Settings
{
    class SettingsContractRandom : SettingsContract
    {
        private readonly byte _randomNumber;

        public byte RandomNumber { get { return _randomNumber; } }

        public SettingsContractRandom(ByteRange range, byte maxAttempts) : base(range, maxAttempts)
        {
            _randomNumber = (byte)new Random().Next(range.From, range.To);
        }
    }
}
