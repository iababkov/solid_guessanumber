﻿namespace SOLID_GuessANumber.Settings
{
    interface IRange<T>
    {
        T From { get; set; }

        T To { get; set; }

        bool Validate();
    }
}
