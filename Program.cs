﻿using SOLID_GuessANumber.Settings;
using SOLID_GuessANumber.IO;
using SOLID_GuessANumber.Main;

namespace SOLID_GuessANumber
{
    class Program
    {
        static void Main()
        {
            var settings = new InitSettings(new ConsoleReadNumber()).Settings;

            new Game(settings).Start();
        }
    }
}
